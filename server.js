var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');

server.listen(8890);
io.on('connection', function (socket) {
    console.log('new client connected');
    var redisClient = redis.createClient();
    redisClient.subscribe('message');
    redisClient.on("message", function(channel, message) {
        console.log("new message in queue ", channel, message);
        socket.emit(channel, message);
    });

    console.log('dashboard update');
    redisClient.subscribe('dashboard');
    redisClient.on("dashboard", function(channel, message) {
        console.log("dashboard update queue ", channel, message);
        socket.emit(channel, message);
    });

    console.log('STP update');
    redisClient.subscribe('stpupdate');
    redisClient.on("stpupdate", function(channel, message) {
        console.log("stpupdate queue ", channel, message);
        socket.emit(channel, message);
    });

    console.log('Project update');
    redisClient.subscribe('projectupdate');
    redisClient.on("projectupdate", function(channel, message) {
        console.log("projectupdate queue ", channel, message);
        socket.emit(channel, message);
    });

    socket.on('disconnect', function() {
        redisClient.quit();
    });
});
